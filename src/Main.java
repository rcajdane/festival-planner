import objecten.Festival;
import gui.GUI;


public class Main {

	public static void main(String args[])
	{
		new Main();
	}
	
	public Main()
	{
		Festival festival = new Festival("The Flying Dutch");
		GUI gui = new GUI(festival);
		gui.setVisible(true);
	}
	
}
