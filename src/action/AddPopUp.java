package action;

import gui.GUI;
import gui.PopUp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;

import objecten.Artist;
import objecten.Festival;
import objecten.Stage;
import objecten.Stand;

public class AddPopUp extends PopUp {

	private static final long serialVersionUID = -7606827078045700790L;
	public Festival festival;
	public GUI mainUI;
	
	public AddPopUp(GUI owner, String name, Festival festival){
		super(owner, name, 600, 600,festival);
		this.festival = festival;
		this.mainUI = owner;
	}
	
	public JPanel menuAdd()
	{
		
		JPanel panelAddMenu = new JPanel();
		panelAddMenu.setName("panelAddMenu");
		panelAddMenu.setBounds(30,50,495,490);
		panelAddMenu.setLayout(null);
		panelAddMenu.setBackground(Color.white);
		
		JButton addArtistBTN = buttons.formButton("Artist",this);
		JButton addStageBTN = buttons.formButton("Stage",this);
		JButton addPerformanceBTN = buttons.formButton("Performance",this);
		JButton addStandBTN = buttons.formButton("Stand",this);
		
		addArtistBTN.setBounds(90,90,150,150);
		addStageBTN.setBounds(250,90,150,150);
		addPerformanceBTN.setBounds(90,250,150,150);
		addStandBTN.setBounds(250,250,150,150);
		
					
		panelAddMenu.add(addArtistBTN);
		panelAddMenu.add(addStageBTN);
		panelAddMenu.add(addPerformanceBTN);
		panelAddMenu.add(addStandBTN);	
		
		return panelAddMenu;
	}
	
	
	public void setFormInPanel(String name, PopUp thisPopUp)
	{
		
       for(Component c : getContentPane().getComponents())
       {
    	   
    	   if(c.getName() == "panelAddMenu")
    	   {
    		   c.setVisible(false);    		   
    		   
    	   		switch(name)
    	   		{
    	   			case "Artist" :    
    	   				getContentPane().remove(getContentPane().getComponentAt(50,50));
    	   				getContentPane().add(artistPanel());
    	   				break;
    	   			case "Stage" :
    	   				getContentPane().remove(getContentPane().getComponentAt(50,50));
    	   				getContentPane().add(stagePanel());
    	   				break;
    	   			case "Performance" :
    	   				getContentPane().remove(getContentPane().getComponentAt(50,50));
    	   				getContentPane().add(performancePanel());
    	   				break;
    	   			case "Stand" :
    	   				getContentPane().remove(getContentPane().getComponentAt(50,50));
    	   				getContentPane().add(standPanel());
    	   				break;
    	   		}
    	   }
       }
	}

	private JPanel artistPanel() 
	{
		JPanel artistPanel = new JPanel();
		artistPanel.setName("artiestPanel");
		artistPanel.setBounds(30,50,495,280);
		artistPanel.setLayout(null);
		artistPanel.setBackground(Color.white);
		
		JLabel titleLabel = new JLabel("Artist");
		titleLabel.setFont(new Font("Sans", Font.BOLD, 20));
		
		JLabel nameLabel = new JLabel("Name");
		JLabel popularityLabel = new JLabel("Popularity");
		JLabel genreLabel = new JLabel("Genre");
		
		JLabel invis = new JLabel();
		invis.setVisible(false);
		
		GridLayout layout = new GridLayout(6,2);
		layout.setHgap(20);
		layout.setVgap(20);
		artistPanel.setLayout(layout);
		
		JTextField setNameText = new JTextField();
		JTextField setPopularText = new JTextField();
		JTextField setGenreText = new JTextField();

		setNameText.setBorder(BorderFactory.createLineBorder(Color.black));
		setPopularText.setBorder(BorderFactory.createLineBorder(Color.black));
		setGenreText.setBorder(BorderFactory.createLineBorder(Color.black));
		
	    JButton back = new JButton();		    
	    back.setText("Back");
	    back.setBackground(new Color(217,118,39));
	    back.setForeground(Color.white);
	    back.setBorderPainted(false);
	    back.setFocusPainted(false);
	    back.setCursor(new Cursor(Cursor.HAND_CURSOR));
	    back.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	back.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	back.setBackground(new Color(217,118,39));
            }
        });
        
	    back.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            { 
   				getContentPane().remove(getContentPane().getComponentAt(50,50));
   				getContentPane().repaint();
   				getContentPane().add(menuAdd());
            }
        });
	    
	    JButton submit = new JButton();		    
        submit.setText("Add");
        submit.setBackground(new Color(217,118,39));
        submit.setForeground(Color.white);
        submit.setBorderPainted(false);
        submit.setFocusPainted(false);
        submit.setCursor(new Cursor(Cursor.HAND_CURSOR));
        submit.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	submit.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	submit.setBackground(new Color(217,118,39));
            }
        });
        
        submit.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            { 
            	String errorCode = "";
            	if(setNameText.getText().isEmpty())
            	{
            		errorCode = "Name";	
            	}
            	else if(setPopularText.getText().isEmpty())
            	{
            		errorCode = "Popularity";	
            	}
            	else if(setGenreText.getText().isEmpty())
            	{
            		errorCode = "Genre";	
            	}
            	else if(errorCode != "")
            	{
            		JOptionPane.showMessageDialog(null,"Er is geen " + errorCode + " ingevuld!","Foutmelding",JOptionPane.WARNING_MESSAGE);	
            	}
            	else
            	{
            		try
            		{ 
            			String splitSpace = setPopularText.getText().replace(" ","");
            			int popularityInt = Integer.parseInt(splitSpace);
            			Artist artist = new Artist(setNameText.getText(), popularityInt, setGenreText.getText());
            			festival.addArtist(artist);
            			
            			System.out.println("Artist: " + artist.getName() + " is born!");
            			
                		AddPopUp.this.getParent().setEnabled(true);
                		AddPopUp.this.dispose();
                			
            		}
            		catch(NumberFormatException e) 
            		{ 
            	    	JOptionPane.showMessageDialog(null,"Popularity is not a number","Foutmelding",JOptionPane.WARNING_MESSAGE);
            	    }	
            	}
            	
            }
        });
		artistPanel.add(titleLabel);
		artistPanel.add(invis);
		artistPanel.add(nameLabel);
		artistPanel.add(setNameText);
		artistPanel.add(popularityLabel);
		artistPanel.add(setPopularText);
		artistPanel.add(genreLabel);
		artistPanel.add(setGenreText);
		artistPanel.add(back);
		artistPanel.add(submit);
		
		return artistPanel;
	}
	
	private JPanel stagePanel() 
	{
		JPanel stagePanel = new JPanel();
		stagePanel.setName("stagePanel");
		stagePanel.setBounds(30,50,495,280);
		stagePanel.setLayout(null);
		stagePanel.setBackground(Color.white);
		
		JLabel titleLabel = new JLabel("Stage");
		titleLabel.setFont(new Font("Sans", Font.BOLD, 20));
		
		JLabel nameLabel = new JLabel("Name");
		JLabel typeLabel = new JLabel("Type");
		
		JLabel invis = new JLabel();
		invis.setVisible(false);
		
		GridLayout layout = new GridLayout(6,2);
		layout.setHgap(20);
		layout.setVgap(20);
		stagePanel.setLayout(layout);
		
		JTextField setNameText = new JTextField();
		JTextField setTypeText = new JTextField();

		setNameText.setBorder(BorderFactory.createLineBorder(Color.black));
		setTypeText.setBorder(BorderFactory.createLineBorder(Color.black));
		
	    JButton back = new JButton();		    
	    back.setText("Back");
	    back.setBackground(new Color(217,118,39));
	    back.setForeground(Color.white);
	    back.setBorderPainted(false);
	    back.setFocusPainted(false);
	    back.setCursor(new Cursor(Cursor.HAND_CURSOR));
	    back.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	back.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	back.setBackground(new Color(217,118,39));
            }
        });
        
	    back.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            { 
   				getContentPane().remove(getContentPane().getComponentAt(50,50));
   				getContentPane().repaint();
   				getContentPane().add(menuAdd());
            }
        });
	    
	    JButton submit = new JButton();		    
        submit.setText("Add");
        submit.setBackground(new Color(217,118,39));
        submit.setForeground(Color.white);
        submit.setBorderPainted(false);
        submit.setFocusPainted(false);
        submit.setCursor(new Cursor(Cursor.HAND_CURSOR));
        submit.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	submit.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	submit.setBackground(new Color(217,118,39));
            }
        });
        
        submit.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            { 
            	String errorCode = "";
            	if(setNameText.getText().isEmpty())
            	{
            		errorCode = "Name";	
            	}
            	else if(setTypeText.getText().isEmpty())
            	{
            		errorCode = "Type";	
            	}
            	else if(festival.getStage().size() > 9)
            	{
            		errorCode = "To many stages!, Meltdown!";
            		
            		JOptionPane.showMessageDialog(null, errorCode ,"Foutmelding",JOptionPane.WARNING_MESSAGE);	
            	}
            	else if(errorCode != "")
            	{
            		JOptionPane.showMessageDialog(null,"Er is geen " + errorCode + " ingevuld!","Foutmelding",JOptionPane.WARNING_MESSAGE);	
            	}
            	else
            	{
            		
            			Stage stage = new Stage(setNameText.getText(), setTypeText.getText());
            			festival.addStage(stage);
            			System.out.println("Stage: " + stage.getName() + " has been forged!");
                			
            			AddPopUp.this.getParent().setEnabled(true);
            			
            			mainUI.paintPageBar();
            			
            			AddPopUp.this.dispose();
                			

            	}
            	
            }
        });
        
        stagePanel.add(titleLabel);
        stagePanel.add(invis);
        stagePanel.add(nameLabel);
		stagePanel.add(setNameText);
		stagePanel.add(typeLabel);
		stagePanel.add(setTypeText);
		stagePanel.add(back);
		stagePanel.add(submit);
		
		return stagePanel;
	}
	
	private JPanel performancePanel() 
	{
		JPanel performancePanel = new JPanel();
		performancePanel.setName("performancePanel");
		performancePanel.setBounds(30,50,495,280);
		performancePanel.setLayout(null);
		performancePanel.setBackground(Color.white);
		
		JLabel titleLabel = new JLabel("Optreden");
		titleLabel.setFont(new Font("Sans", Font.BOLD, 20));
		
		JLabel nameLabel = new JLabel("Name");
		JLabel beginLabel = new JLabel("Begintime");
		JLabel endLabel = new JLabel("Endtime");
		JLabel artistLabel = new JLabel("Artist");
		
		JLabel invis = new JLabel();
		invis.setVisible(false);
		
		GridLayout layout = new GridLayout(6,2);
		layout.setHgap(20);
		layout.setVgap(20);
		performancePanel.setLayout(layout);
		
		JTextField setNameText = new JTextField();
		JSpinner setBeginTimeText = new JSpinner( new SpinnerDateModel() );
		JSpinner.DateEditor beginTimeEditor = new JSpinner.DateEditor(setBeginTimeText, "HH:mm");
		setBeginTimeText.setEditor(beginTimeEditor);
		setBeginTimeText.setValue(new Date());
		
		JSpinner setEndTimeText = new JSpinner( new SpinnerDateModel() );
		JSpinner.DateEditor endTimeEditor = new JSpinner.DateEditor(setEndTimeText, "HH:mm");
		setEndTimeText.setEditor(endTimeEditor);
		setEndTimeText.setValue(new Date());

		JComboBox<String> artistList = new JComboBox<String>();
		for(Artist artist : festival.getArtists())
		{
			artistList.addItem(artist.getName());
		}
		
		
		setNameText.setBorder(BorderFactory.createLineBorder(Color.black));
		setBeginTimeText.setBorder(BorderFactory.createLineBorder(Color.black));
		setEndTimeText.setBorder(BorderFactory.createLineBorder(Color.black));
		artistList.setBorder(BorderFactory.createLineBorder(Color.black));
		
	    JButton back = new JButton();		    
	    back.setText("Back");
	    back.setBackground(new Color(217,118,39));
	    back.setForeground(Color.white);
	    back.setBorderPainted(false);
	    back.setFocusPainted(false);
	    back.setCursor(new Cursor(Cursor.HAND_CURSOR));
	    back.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	back.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	back.setBackground(new Color(217,118,39));
            }
        });
        
	    back.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            { 
   				getContentPane().remove(getContentPane().getComponentAt(50,50));
   				getContentPane().repaint();
   				getContentPane().add(menuAdd());
            }
        });
	    
	    JButton submit = new JButton();		    
        submit.setText("Add");
        submit.setBackground(new Color(217,118,39));
        submit.setForeground(Color.white);
        submit.setBorderPainted(false);
        submit.setFocusPainted(false);
        submit.setCursor(new Cursor(Cursor.HAND_CURSOR));
        submit.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	submit.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	submit.setBackground(new Color(217,118,39));
            }
        });
        
        submit.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            { 
            	String errorCode = "";
            	if(setNameText.getText().isEmpty())
            	{
            		errorCode = "Naam";	
            	}
            	else if(setBeginTimeText.getValue().equals(null))
            	{
            		errorCode = "Begin time";	
            	}
            	else if(setEndTimeText.getValue().equals(null))
            	{
            		errorCode = "End time";	
            	}
            	else if(errorCode != "")
            	{
            		JOptionPane.showMessageDialog(null,"No " + errorCode + " found!","Errormessage",JOptionPane.WARNING_MESSAGE);	
            	}
            	else
            	{

            		Date beginDate = (Date)setBeginTimeText.getValue();
            		Date endDate = (Date)setEndTimeText.getValue();
            		
            			long beginTime = beginDate.getTime();
            			long endTime = endDate.getTime();
            			
            			System.out.println("Begin: " + beginTime + " \n End: " + endTime); 
            			
            			
            			//Optreden optreden = new Optreden(beginTijd, eindTijd, setNaam.getText());
            			
            			/*
            			for(Artiest artiest : festival.getArtiesten())
            			{
            				if(artistList.getSelectedItem() == artiest.getNaam())
            				{
            					optreden.addArtiest(artiest);
            				}
            			}*/
            			            			
            			//System.out.println(optreden.toString());
                		
            			
            			AddPopUp.this.getParent().setEnabled(true);
            			AddPopUp.this.dispose();
                			
            	
            	}
            	
            }
        });
        
        
        performancePanel.add(titleLabel);
        performancePanel.add(invis);
        performancePanel.add(nameLabel);
        performancePanel.add(setNameText);
        performancePanel.add(beginLabel);
        performancePanel.add(setBeginTimeText);
        performancePanel.add(endLabel);
        performancePanel.add(setEndTimeText);
        performancePanel.add(artistLabel);
        performancePanel.add(artistList);
        performancePanel.add(back);
        performancePanel.add(submit);
		
		return performancePanel;
	}
	
	private JPanel standPanel() 
	{
		JPanel standPanel = new JPanel();
		standPanel.setName("standPanel");
		standPanel.setBounds(30,50,495,280);
		standPanel.setLayout(null);
		standPanel.setBackground(Color.white);
		
		JLabel titleLabel = new JLabel("Stand");
		titleLabel.setFont(new Font("Sans", Font.BOLD, 20));
		
		JLabel nameLabel = new JLabel("Name");
		JLabel capacityLabel = new JLabel("Capacity");
		JLabel typeLabel = new JLabel("Type");
		JLabel popularityLabel = new JLabel("Popularity");
		
		JLabel invis = new JLabel();
		invis.setVisible(false);
		
		GridLayout layout = new GridLayout(6,2);
		layout.setHgap(20);
		layout.setVgap(20);
		standPanel.setLayout(layout);
		
		JTextField setNameText = new JTextField();
		JTextField setPopularityText = new JTextField();
		JTextField setCapacityText = new JTextField();
		JTextField setTypeText = new JTextField();

		setNameText.setBorder(BorderFactory.createLineBorder(Color.black));
		setPopularityText.setBorder(BorderFactory.createLineBorder(Color.black));
		setCapacityText.setBorder(BorderFactory.createLineBorder(Color.black));
		setTypeText.setBorder(BorderFactory.createLineBorder(Color.black));
		
	    JButton back = new JButton();		    
	    back.setText("Back");
	    back.setBackground(new Color(217,118,39));
	    back.setForeground(Color.white);
	    back.setBorderPainted(false);
	    back.setFocusPainted(false);
	    back.setCursor(new Cursor(Cursor.HAND_CURSOR));
	    back.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	back.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	back.setBackground(new Color(217,118,39));
            }
        });
        
	    back.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            { 
   				getContentPane().remove(getContentPane().getComponentAt(50,50));
   				getContentPane().repaint();
   				getContentPane().add(menuAdd());
            }
        });
	    
	    JButton submit = new JButton();		    
        submit.setText("Add");
        submit.setBackground(new Color(217,118,39));
        submit.setForeground(Color.white);
        submit.setBorderPainted(false);
        submit.setFocusPainted(false);
        submit.setCursor(new Cursor(Cursor.HAND_CURSOR));
        submit.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	submit.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	submit.setBackground(new Color(217,118,39));
            }
        });
        
        submit.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            { 
            	String errorCode = "";
            	if(setNameText.getText().isEmpty())
            	{
            		errorCode = "Name";	
            	}
            	else if(setPopularityText.getText().isEmpty())
            	{
            		errorCode = "Popularity";	
            	}
            	else if(setCapacityText.getText().isEmpty())
            	{
            		errorCode = "Capacity";	
            	}
            	else if(setTypeText.getText().isEmpty())
            	{
            		errorCode = "Type";	
            	}
            	else if(errorCode != "")
            	{
            		JOptionPane.showMessageDialog(null,"No " + errorCode + " found!","Errormessage",JOptionPane.WARNING_MESSAGE);	
            	}
            	else
            	{

            		String splitSpace = setPopularityText.getText().replace(" ","");
        			int popularityInt = Integer.parseInt(splitSpace);
        			splitSpace = setCapacityText.getText().replace(" ","");
        			int capacityInt = Integer.parseInt(splitSpace);           			
            			
        			Stand stand = new Stand(setNameText.getText(), popularityInt, capacityInt, setTypeText.getText());
        			festival.addStand(stand);
        			
        			System.out.println("Stand: " + stand.getName() + " is born!");
            			
            			AddPopUp.this.getParent().setEnabled(true);
            			AddPopUp.this.dispose();
                			
            	
            	}
            	
            }
        });
        
        standPanel.add(titleLabel);
        standPanel.add(invis);
        standPanel.add(nameLabel);
        standPanel.add(setNameText);
        standPanel.add(popularityLabel);
        standPanel.add(setPopularityText);
        standPanel.add(capacityLabel);
        standPanel.add(setCapacityText);
        standPanel.add(typeLabel);
        standPanel.add(setTypeText);
        standPanel.add(back);
        standPanel.add(submit);
		
		return standPanel;
	}
	

}
