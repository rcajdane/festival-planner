package fileIO;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import objecten.Festival;

public class LoadStream {
	
	private Festival festival;
	
	public Festival readFile(File file) throws ClassNotFoundException{
        try{

            ObjectInputStream in1 = new ObjectInputStream(new FileInputStream(file));
            // (ArrayList) in1.readObject();
            if(in1.readInt() == 1)
            {
            	System.out.println("IT WORKS");
            }
            this.festival = (Festival)in1.readObject();
            in1.close();

        }catch(IOException ex){
            ex.printStackTrace();
        }
        
        return festival;
    }

}
