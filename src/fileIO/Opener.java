package fileIO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import objecten.Festival;

public class Opener  {
	
	private Festival festival;

	//@Override
	public void openFile() throws ClassNotFoundException {
		// TODO Auto-generated method stub
		JFileChooser opener = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Festival Planner Data", "fpl");
        opener.setFileFilter(filter);
        //REPLACE null with the main JFrame!
        int returnVal = opener.showOpenDialog(SwingUtilities.getWindowAncestor(opener));
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            
        	LoadStream loadstream = new LoadStream();
        	this.festival = loadstream.readFile(opener.getSelectedFile());
        	
            //filename.setText(c.getSelectedFile().getName());
            //dir.setText(c.getCurrentDirectory().toString());
        }
        if (returnVal == JFileChooser.CANCEL_OPTION) {
            
        }
	}
	
	public Festival getFestivalObject()
	{
		return festival;
	}
	

}
