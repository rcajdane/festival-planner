package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fileIO.Opener;
import fileIO.Saver;
import objecten.Festival;
import action.AddPopUp;

public class Button {

	public Festival festival;
	
	public Button(Festival festival)
	{
		this.festival = festival;
	}
	
	public JButton titleBarButton(String name)
	{
		final Icon normal = new ImageIcon("src/images/" + name + ".png");
        final Icon hover = new ImageIcon("src/images/" + name + "Hover.png");
	    
        final JButton button = new JButton();
        button.setName(name);
        button.setIcon(normal);
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
	    
        button.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	button.setIcon(hover);
            }

            public void mouseExited(MouseEvent evt) {
            	button.setIcon(normal);
            }
        });
        
        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
            	
            	
            	switch(((JComponent) event.getSource()).getName())
            	{
                	case "exit":
                		if(SwingUtilities.getWindowAncestor(button) instanceof JDialog)
                		{
                			JDialog dialog = (JDialog)SwingUtilities.getWindowAncestor(button);
                			dialog.getParent().setEnabled(true);
                			dialog.dispose();
                			
                		}
                		else if(SwingUtilities.getWindowAncestor(button) instanceof JFrame)
                		{
                			System.exit(0);
                		}
                		break;
                	case "minimize":
                		JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(button);
                		frame.setState(Frame.ICONIFIED);
                		break;
                		
            	}
            }
        });
	    
	   return button; 
	}
	
	public JButton menuButton(String name, GUI gui)
	{
		final Icon normal = new ImageIcon("src/images/" + name + ".png");
        final Icon hover = new ImageIcon("src/images/" + name + "Hover.png");
	    
        final JButton button = new JButton();
        button.setName(name);
        button.setIcon(normal);
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
	    
        button.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	button.setIcon(hover);
            }

            public void mouseExited(MouseEvent evt) {
            	button.setIcon(normal);
            }
        });
        
        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
            	switch(((JComponent) event.getSource()).getName())
            	{
            		case "open":
					//Use the Opener.getFestivalObject method to get the retrieved Festival Object from the chosen file for later use
            		try {
						new Opener().openFile();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            			
            			break;
            		case "save":
            			//Please check if the right festival object is inserted in the line below.
            			new Saver().saveFile(festival);
            			
            			break;
                	case "add":
                		AddPopUp addDialog = new AddPopUp(gui, "Add", festival);
                		addDialog.getContentPane().add(addDialog.menuAdd());
                		break;
                	case "edit":
                		break;
                	case "delete":
                		break;
            	}
            }
        });
	    
	   return button; 
	}
	
	public JButton tabButton(String name)
	{
	    
        final JButton button = new JButton();
        button.setName(name);
        button.setText(name);
        button.setPreferredSize(new Dimension(100, 45));
        button.setBackground(new Color(217,118,39));
        button.setForeground(Color.white);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
        button.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	button.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	button.setBackground(new Color(217,118,39));
            }
        });
        
        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
            	switch(((JComponent) event.getSource()).getName())
            	{
            	case "" :
            		break;
            	}
            	
            	System.out.println(((JComponent)event.getSource()).getName());
            	
            }
        });
	    
	   return button; 
	}

	public JButton formButton(String name, AddPopUp addPopUp)
	{
	    
        final JButton button = new JButton();
        button.setName(name);
        button.setText(name);
        
        button.setBackground(new Color(217,118,39));
        button.setForeground(Color.white);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
        button.addMouseListener(new MouseAdapter() 
        {
            public void mouseEntered(MouseEvent evt) {
            	button.setBackground(new Color(231,163,54));
            }

            public void mouseExited(MouseEvent evt) {
            	button.setBackground(new Color(217,118,39));
            }
        });
        
        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {

            		addPopUp.setFormInPanel(name,addPopUp);
            }
        });
	    
	   return button; 
	}

}
