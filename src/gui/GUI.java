package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.RoundRectangle2D;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import objecten.Festival;
import objecten.Stage;

public class GUI extends JFrame {

	private static final long serialVersionUID = -3195409754010277497L;

	protected int xMouse;
    protected int yMouse;
    public Button buttons;
    public Festival festival;
	
    private JPanel pageBar;
    private JPanel contentPanel;
    
	public GUI(Festival festival)
	{
		super("Festival Planner");
		this.festival = festival;
		buttons = new Button(festival);
			
		initializeFrame();
		initializeTitleBar();
		initializeMenu();
	    initializeComponent();
	}
	
	public void initializeFrame()
	{
		ImageIcon icon = new ImageIcon("src/images/mainIcon.png");
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setUndecorated(true);
	    setLocationRelativeTo(null);
	    setShape(new RoundRectangle2D.Double(5, 5, 1250, 850, 20, 20));
	    setSize(1300,900);
	    setIconImage(icon.getImage());
	    
	    JPanel mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        mainPanel.setLayout(null);
        mainPanel.setBackground(Color.WHITE);
	    
	    setContentPane(mainPanel);
	}
		
	public void initializeTitleBar()
	{
        JPanel menuPanel = new JPanel();
        menuPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        menuPanel.setLayout(null);
        menuPanel.setBounds(0,0,1300,32);
        
        menuPanel.addMouseMotionListener(new MouseMotionAdapter() 
        {
            public void mouseDragged(MouseEvent evt) 
            {
                int x = evt.getXOnScreen();
                int y = evt.getYOnScreen();
                
                setLocation(x - xMouse,y - yMouse);
            }
        });
        
        menuPanel.addMouseListener(new MouseAdapter() 
        {
            public void mousePressed(MouseEvent evt) 
            {
              xMouse = evt.getX();
              yMouse = evt.getY();
            }
        });            
        
        JLabel title = new JLabel("Festival Planner");
        JButton exit = buttons.titleBarButton("exit");
        JButton minimize = buttons.titleBarButton("minimize");
        
        exit.setBounds(1200,0,50,32);
        minimize.setBounds(1150,0,50,32); 
        title.setBounds(15,2,100,30);
        
        menuPanel.add(exit);
        menuPanel.add(minimize);
        menuPanel.add(title);
        
        getContentPane().add(menuPanel);
	}
	
	public void initializeMenu()
	{
		JButton open = buttons.menuButton("open", GUI.this);
		JButton save = buttons.menuButton("save", GUI.this);
		JButton add = buttons.menuButton("add", GUI.this);
        JButton edit = buttons.menuButton("edit", GUI.this);
        JButton delete = buttons.menuButton("delete", GUI.this);
        
        open.setBounds(10,145,38,38);
        save.setBounds(52,145,38,38);
        add.setBounds(10,187,80,80);
        edit.setBounds(10,271,80,80); 
        delete.setBounds(10,355,80,80);
        
        getContentPane().add(open);
        getContentPane().add(save);
        getContentPane().add(add);
        getContentPane().add(edit);
        getContentPane().add(delete);	
		
	}
	
	
	
	public void initializeComponent()
	{	
		
			Object columnNames[] = { "Optreden", "Artiesten", "Begintijd", "Eindtijd" };
			
			Object rowData[][] = 
				{ 
					 {new Integer(1), "Hardwell", "10:00", "12:30"},
			    	 {new Integer(2), "Yellow Clow", "13:00", "14:00"},
			    	 {new Integer(3), "Vicetone", "14:30", "16:00"},
			    	 {new Integer(4), "Dzeko & torres", "16:15", "17:00"},
			    	 {new Integer(5), "Fedde leGrand", "17:15", "18:00"}
				};
			
			pageBar = new JPanel(new FlowLayout(FlowLayout.LEFT,0,0));
			pageBar.setName("pageBar");
		    pageBar.setPreferredSize(new Dimension(1120, 49));
		    pageBar.setBackground(Color.white);
		    
			contentPanel = new JPanel(new BorderLayout());
			contentPanel.setName("contentPanel");
			contentPanel.setBounds(100,50,1100,770);
			contentPanel.setBackground(Color.white);
			
			JTable table = new JTable(rowData, columnNames);
		    JScrollPane scrollPane = new JScrollPane(table);
		    scrollPane.setPreferredSize(new Dimension(1120, 549));
		    
		    festival.addStage(new Stage("Main Stage", "main"));
		    
		    if(!festival.getStage().isEmpty())
		    {
		    	for(Stage stage : festival.getStage())
				{
		    		pageBar.add(buttons.tabButton(stage.getName()));
				}
		    }

	        contentPanel.add(pageBar,BorderLayout.NORTH);
		    contentPanel.add(scrollPane,BorderLayout.CENTER);

		    getContentPane().add(contentPanel);   	    	    
	}
	
	
	public void paintPageBar()
	{		       
		pageBar.removeAll();
		
		if(!festival.getStage().isEmpty())
		{
			for(Stage stage : festival.getStage())
			{
				pageBar.add(buttons.tabButton(stage.getName()));
				
			}
		}
		
		pageBar.validate();
		pageBar.repaint();
		    
	}
	/*
	public void repaintPageBar()
	{
		//paintPageBar();
		SwingUtilities.getDeepestComponentAt(this, 1160, 90);
	}*/

}
