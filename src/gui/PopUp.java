package gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.RoundRectangle2D;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import objecten.Festival;

public class PopUp extends JDialog 
{

	private static final long serialVersionUID = -1106203478449176213L;
	protected int xMouse;
    protected int yMouse;
    protected Button buttons;
	
	public PopUp(JFrame owner, String naam, int Width, int Height, Festival festival)
	{
		super(owner, naam);
		buttons = new Button(festival);
		setName(naam);
		initializeFrame(owner, Width, Height);
		initializeTitleBar(naam);
		
		setVisible(true);
		
	}
		
	public void initializeFrame(JFrame owner, int Width, int Height)
	{
		ImageIcon icon = new ImageIcon("src/images/mainIcon.png");
		
		owner.setEnabled(false);
		
	    setUndecorated(true);
	    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(owner);
	    setShape(new RoundRectangle2D.Double(5, 5, Width - 50, Height - 50, 20, 20));
	    setSize(Width,Height);
	    setIconImage(icon.getImage());
	    
	    JPanel mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        mainPanel.setLayout(null);
        mainPanel.setBackground(Color.white);
	    
	    setContentPane(mainPanel);
	}
		
	public void initializeTitleBar(String naam)
	{
        JPanel menuPanel = new JPanel();
        menuPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        menuPanel.setLayout(null);
        menuPanel.setBounds(0,0,600,32);
        
        
        menuPanel.addMouseMotionListener(new MouseMotionAdapter() 
        {
            public void mouseDragged(MouseEvent evt) 
            {
                int x = evt.getXOnScreen();
                int y = evt.getYOnScreen();
                
                setLocation(x - xMouse,y - yMouse);
            }
        });
        
        menuPanel.addMouseListener(new MouseAdapter() 
        {
            public void mousePressed(MouseEvent evt) 
            {
              xMouse = evt.getX();
              yMouse = evt.getY();
            }
        });            
        
        JLabel title = new JLabel(naam);
        JButton exit = buttons.titleBarButton("exit");
        
        exit.setBounds(500,0,50,32);
        title.setBounds(15,2,100,30);
        
        menuPanel.add(exit);
        menuPanel.add(title);
        
        getContentPane().add(menuPanel);
	}
		
}
