package objecten;
import java.util.ArrayList;


public class Festival {
	
	public String name;
	private ArrayList<Stage> stages;
	private ArrayList<Stand> stands;
	public ArrayList<Artist> artists;
	
	public Festival(String name)
	{
		this.name = name;
		this.artists = new ArrayList<Artist>();
		this.stands = new ArrayList<Stand>();
		this.stages = new ArrayList<Stage>();
	}
	
	// Add to arraylist
	public void addStage(Stage stage)
	{
		stages.add(stage);
	}
	
	public void addStand(Stand stand)
	{
		stands.add(stand);
	}
	
	public void addArtist(Artist artist)
	{
		artists.add(artist);
	}
	
	public ArrayList<Stage> getStage()
	{
		return stages;
	}
	
	public ArrayList<Artist> getArtists()
	{
		return artists;
	}
	
	public ArrayList<Stand> getStands()
	{
		return stands;
	}

	@Override
	public String toString()
	{
		return "Festival [name=" + name + ", "
				+ "stands=" + stands + ", "
				+ "artists=" + artists + "]";
	}
	
	
	
}
