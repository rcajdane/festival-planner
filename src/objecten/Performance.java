package objecten;
import java.util.ArrayList;


public class Performance {

	private Time time;
	private ArrayList<Artist> artists;
	
	public Performance(int begin, int end, String name)
	{
		addTime(begin,end);

	}
	
	public void addArtist(Artist artist)
	{
		artists.add(artist);
	}
	
	public void addTime(int begin, int eind)
	{
		time.setTime(begin, eind);
	}
	
	public ArrayList<Artist> getArtist()
	{
		return artists;
	}
	
	public Time getTime()
	{
		return time;
	}

	public String toString() {
		String artistString = "";
		for(Artist artist : artists)
		{
			artistString += artist + "||";
		}
		
		return "Performance [Time=" + time + ", Artists=" + artistString + "]";
	}
		
}
