package objecten;
import java.util.ArrayList;


public class Stage extends Building{

	private ArrayList<Performance> lineUp;
	
	public Stage(String name, String type)
	{	
		setName(name);	
	}
	
	public void addPerformance(int begin, int end, String name)
	{
		lineUp.add(new Performance(begin,end,name));
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	
	public String getName()
	{
		return name;
	}
	
	public ArrayList<Performance> getLineUp()
	{
		return lineUp;
	}

	@Override
	public String toString() {
		return "Stage [Name=" + name + ", "
				+ "lineUp=" + lineUp + "]";
	}
	
	

	
	
}
