package objecten;


public class Stand extends Building {

	private int popularity;
	private int capacity;
	private String type;
	
	public Stand(String name, int capacity, int popularity, String type)
	{
		setName(name);
		setCapacity(capacity);
		setPopularity(popularity);
		setType(type);
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setCapacity(int capacity)
	{
		this.capacity = capacity;
	}
	
	public void setPopularity(int popularity)
	{
		this.popularity = popularity;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getCapacity()
	{
		return capacity;
	}
	
	public int getPopularity()
	{
		return popularity;
	}
	
	public String getType()
	{
		return type;
	}
	
	@Override
	public String toString() {
		return "Stand [Naam=" + name + ", "
				+ "Popularity=" + popularity + ", "
				+ "type=" + type + ", "
				+ "capaciteit=" + capacity + "]";
	}
	
}
