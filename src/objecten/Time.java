package objecten;

public class Time {

	private int beginTime;
	private int endTime;
	
	public Time(int begin, int end)
	{
		this.setTime(begin, end);
	}
	
	public void setTime(int begin, int end)
	{
		this.beginTime = begin;
		this.endTime = end;
	}
	
	public int getBeginTime()
	{
		return beginTime;
	}
	
	public int getEndTime()
	{
		return endTime;
	}

	public String toString() {
		return "Time [Begin=" + beginTime + ", End=" + endTime + "]";
	}
	
	
}
